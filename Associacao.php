<?php
    include_once 'class/ClassFornecedor.php';
    include_once 'class/ClassProduto.php';

    $fornecedor1 = new ClassFornecedor;
    $fornecedor1->codigo = 43;
    $fornecedor1->endereco = "baixa d'egua";
    $fornecedor1->cidade = "fortaleza";
    $fornecedor1->razaoSocial = "baima";

    $produto1 = new Produto;
    $produto1->codigo = 414534;
    $produto1->descricao = "bombom de cafe chupado";
    $produto1->preco = '0,10';
    $produto1->qtn = 3;
    $produto1->fornecedor = $fornecedor1;

    echo 'Código: ' .  $produto1->codigo . "\n";
    echo 'Descricao: ' . $produto1->descricao . "\n";
    echo 'Codigo: ' . $produto1->fornecedor->codigo . "\n";
    echo 'razao social: ' . $produto1->fornecedor->razaoSocial . "\n";
